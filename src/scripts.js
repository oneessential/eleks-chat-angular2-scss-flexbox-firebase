function myScripts(){
	const arrow = document.querySelector('.top-navigation__arrow');
	const aside = document.querySelector('.aside-wrapper');
	const main = document.querySelector('.messages-wrapper');

	//var asideSavedSize = parseInt(window.getComputedStyle(aside,null).width);

	window.onload = function(){
		if(window.innerWidth > 782){
	      aside.classList.add('is-open');
		} else {
		  aside.classList.remove('is-open');
		}
	};

	window.addEventListener('resize', function() {
	  if(window.innerWidth < 780){
	      if(aside.classList.contains('is-open')){
			aside.classList.remove('is-open');
			aside.style.width = '88px';
			main.style.width = 'calc(100% - 88px)';
		  }
	  } else if(window.innerWidth > 780){
		 if(aside.classList.contains('is-open')){
			aside.style.width = '380px';
			main.style.width = 'calc(100% - 380px)';
		  } else {
			aside.classList.add('is-open');
			aside.style.width = '380px';
			main.style.width = 'calc(100% - 380px)';
		  }
	  }
	});

	// responsive
	arrow.addEventListener('click', function(){
	  if(parseInt(window.getComputedStyle(aside,null).width) > 88){
	    aside.classList.add('is-open');
	  }
	  if(aside.classList.contains('is-open')) {
	    aside.classList.remove('is-open');
	    aside.style.width = '88px';
	    main.style.width = 'calc(100% - 88px)';
	  } else {
	    aside.classList.add('is-open');
	    aside.style.width = '380px';
	    main.style.width = 'calc(100% - 380px)';
	  }
	});

	// textarea
	const textarea = document.querySelector('.chat-input__textarea-wrapper textarea');
	const messageForm = document.querySelector('#chat-input__form');

	textarea.addEventListener('keypress', function(e) {
	  if(e.which == 13 && !e.shiftKey) {
	      messageForm.submit();
	      e.preventDefault();
	      return false;
	    }
	});

	const messageTextarea = document.getElementById('message-textarea');
	messageTextarea.addEventListener('keyup', function(){
		let text	= this.value,
			matches	= text.match(/\n/g),
			breaks	= matches ? matches.length : 2,
			lineH	= 20;
		this.style.height =  (breaks * lineH) + 'px';
	});

	//send message button
	const sendMessage = document.querySelector('.chat-input__send-button')

	sendMessage.addEventListener('click', function(){
	  messageForm.submit();
	})

	//scroll chat after submit
	// let messagesHistory = document.querySelector('.messages__history');
	//
	// window.onload = function () {
	//   messagesHistory.scrollTop = messagesHistory.scrollHeight;
	// };

	//add file to send

	const addFileButton = document.querySelector('.chat-input__file')

	addFileButton.addEventListener('click', function(){
	let fileInput = document.querySelector('#chat-input__attach-file');
	fileInput.click();
	});
};