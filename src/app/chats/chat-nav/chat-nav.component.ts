import { Component, AfterViewInit } from '@angular/core';
import { ChatService } from "../shared/chat.service";

@Component({
  selector: 'ct-chat-nav',
  styleUrls: ['./chat-nav.component.scss'],
  templateUrl: './chat-nav.component.html'
})

export class ChatNavComponent implements AfterViewInit {
  constructor(private service: ChatService) {}
  
  private onSearchValueChange(value: string): void {
    this.service.setSearchValue(value);
  }

  ngAfterViewInit(): void {
    let arrow: any = document.querySelector('.top-navigation__arrow');
    let aside: any = document.querySelector('.aside-wrapper');

    arrow.addEventListener('click', function(){
      if(aside.classList.contains('is-open')) {
        aside.classList.remove('is-open');
      } else {
        aside.classList.add('is-open');
      }
	  });

    let onload = function(){
      if(window.innerWidth > 780){
        aside.classList.toggle('is-open', true);
      } else {
        aside.classList.toggle('is-open', false);
      }
    };

    onload();

    let id;
    window.addEventListener('resize', function() {
      clearTimeout(id);
      id = setTimeout(doneResizing, 500);
    });
    let doneResizing = () => {
      if(window.innerWidth < 780) {
        if(aside.classList.contains('is-open')) {
          aside.classList.remove('is-open');
        }
      } 
      else if (window.innerWidth > 780) {
        if (!aside.classList.contains('is-open')) {
          aside.classList.add('is-open');
        }
      }
    }
    // window.addEventListener('resize', function() {
    //   if(window.innerWidth < 780) {
    //     if(aside.classList.contains('is-open')) {
    //       aside.classList.remove('is-open');
    //     }
    //   } 
    //   else if (window.innerWidth > 780) {
    //     if (!aside.classList.contains('is-open')) {
    //       aside.classList.add('is-open');
    //     }
    //   }
    // });
  }

}
