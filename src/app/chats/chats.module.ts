import { NgModule } from '@angular/core';
import { SharedModule }  from '../shared';
import { ChatsComponent } from './chats.component';
import { ChatListComponent } from './chat-list';
import { ChatDetailComponent } from './chat-detail';
import { ChatNewComponent } from './chat-new';
import { ChatNavComponent } from './chat-nav';
import { ChatHeaderComponent } from "./chat-header/chat-header.component";
import { ChatNewHeaderComponent } from "./chat-new-header/chat-new-header.component";


import { ChatHolderComponent } from './chat-holder';
import { ChatsRoutingModule } from './chats-routing.module';
import { MessagesSharedModule } from '../messages';

import { ChatService } from './shared/chat.service';
import { MessageService } from '../messages/shared/message.service';
import { FilterChatByNamePipe } from "../shared/filter-chat-by-name.pipe";
import { FilterUserByUsernamePipe } from "../shared/filter-user-by-username.pipe";

@NgModule({
  declarations: [
    ChatsComponent,
    ChatListComponent,
    ChatDetailComponent,
    ChatNewComponent,
    ChatNewHeaderComponent,
    ChatNavComponent,
    ChatHeaderComponent,
    ChatHolderComponent,
    FilterChatByNamePipe,
    FilterUserByUsernamePipe
  ],
  imports: [
    SharedModule,
    MessagesSharedModule,
    ChatsRoutingModule
  ],
  providers: [ChatService, MessageService]
})

export class ChatsModule {}
