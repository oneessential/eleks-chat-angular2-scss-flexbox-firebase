import { Component } from '@angular/core';
import { MessageService } from "../../messages/shared/message.service";

@Component({
  selector: 'ct-chat-header',
  styleUrls: ['./chat-header.component.scss'],
  templateUrl: './chat-header.component.html'
})

export class ChatHeaderComponent {

  constructor(private messageService: MessageService) {}
  
  private onSearchValueChange(value: string): void {
    this.messageService.setSearchValue(value);
  }
}
