import { Component } from '@angular/core';
import { UsersService } from '../../core/users.service';
import { ChatNewComponent } from "../index";

@Component({
  selector: 'ct-chat-new-header',
  styleUrls: ['./chat-new-header.component.scss'],
  templateUrl: './chat-new-header.component.html'
})

export class ChatNewHeaderComponent {

  constructor(private usersService: UsersService, private chatNewComponent: ChatNewComponent) {}
  
  private onSearchValueChange(value: string): void {
    this.chatNewComponent.onUserSearch();
    this.usersService.setSearchValue(value);
  }

}
