import { Component, OnInit, OnDestroy } from '@angular/core';
import { UsersService } from "../../core/users.service";
import { Subscription } from "rxjs/Subscription";


@Component({
  selector: 'ct-chat-new',
  styleUrls: ['./chat-new.component.scss'],
  templateUrl: './chat-new.component.html'
})

export class ChatNewComponent implements OnInit, OnDestroy {
  public users: any;
  private searchValue: string = '';
  private subscriptions: Subscription[] = [];
  
  constructor(private usersService: UsersService) {}

  ngOnInit() {}

  onUserSearch() {
    this.subscriptions.push(
      this.usersService.getUsers()
        .subscribe(
          users => this.users = users,
          error => console.error('Error' + error)
        )
    );

    this.subscriptions.push(
      this.usersService
        .getSearchValue()
        .subscribe(value => this.searchValue = value)
    );
  }

  ngOnDestroy() {
    this.subscriptions.map(subscription => subscription.unsubscribe())
  }

}
