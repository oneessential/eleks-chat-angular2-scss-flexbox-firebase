import { Component, OnInit, OnDestroy, AfterViewChecked, ElementRef, ViewChild } from '@angular/core';
import { ActivatedRoute, Router, Params } from '@angular/router';
import { MessageService } from '../message.service';
// import { Message } from '../message.model';
import { Subscription } from "rxjs/Subscription";
import { Observable } from 'rxjs';
import {FirebaseListObservable} from "angularfire2";

@Component({
  selector: 'ct-message-list',
  styleUrls: ['./message-list.component.scss'],
  templateUrl: './message-list.component.html'
})

export class MessageListComponent implements OnInit, OnDestroy, AfterViewChecked  {

  private chatId: number;
  public messages;
  private searchValue: string = '';
  private subscription: Subscription;
  public email: string;

  constructor(private route: ActivatedRoute,
              private router: Router,
              private messageService: MessageService) {}

  @ViewChild('scrollable') private messageContainer: ElementRef;

  ngAfterViewChecked() {
    this.scrollToBottom();
  }

  scrollToBottom(): void {
    try {
      this.messageContainer.nativeElement.scrollTop = this.messageContainer.nativeElement.scrollHeight;
    } catch(err) {
      console.log('Failed to scroll messages...');
    }
  }

  ngOnInit(): void {
    this.route.params.subscribe((params: Params) => {
      this.chatId = +params['id'];
      // this.messages = Observable.interval(3000).map(a => ({email: 'fff'}))
      this.messages = this.messageService.messages
        .flatMap(a => Observable.from(a))
        .filter(el => el['chatId'] === this.chatId)
        .scan((a: Array<any>, b) => {a.push(b); return a;}, [])


      // this.messages.subscribe(a => console.log(a))
      // this.messages = this.messageService.messages;
    });

    this.email = this.messageService.email;

    this.messageService
      .getSearchValue()
      .subscribe(value => this.searchValue = value);
  }

  isOutgoing(messageEmail) {
    if (messageEmail == this.email) {
      return true;
    }
    return false;
  }

  ngOnDestroy(): void {
    // this.subscription.unsubscribe();
  }

}
