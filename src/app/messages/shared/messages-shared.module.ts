import { NgModule } from '@angular/core';
import { SharedModule }  from '../../shared';
import { MessageListComponent } from './message-list';
import { MessageNewComponent } from './message-new';

import { HighlightDirective } from '../../shared/highlight.directive';
import { FilterMessageByTextPipe } from "./filter-messages-by-text.pipe";

@NgModule({
  declarations: [
    MessageListComponent,
    MessageNewComponent,
    HighlightDirective,
    FilterMessageByTextPipe
  ],
  imports: [
    SharedModule
  ],
  exports: [
    MessageListComponent,
    MessageNewComponent
  ],
  providers: []
})

export class MessagesSharedModule {}
