import { Injectable } from '@angular/core';
// import { MESSAGES } from './mock-messages';
import { Message } from "./message.model";
import { BehaviorSubject } from 'rxjs';
import {AngularFire, AuthProviders, AuthMethods, FirebaseListObservable} from 'angularfire2';

@Injectable()
export class MessageService {
  public messages: FirebaseListObservable<any>;
  public users: FirebaseListObservable<any>;
  public displayName: string;
  public email: string;
  public photoURL: string;

  private search$: BehaviorSubject<string> = new BehaviorSubject('');
  
  constructor(public af: AngularFire) {
    this.af.auth.subscribe(auth => { 
      // console.log(auth);
      if (auth) {
        if (auth.facebook != undefined) {
          this.displayName = auth.facebook.displayName;
          this.email = auth.facebook.email;
          this.photoURL = auth.facebook.photoURL;
        } else if(auth && auth.auth.displayName != null && auth.auth.email != null) {
          this.displayName = auth.auth.displayName;
          this.email = auth.auth.email;
          this.photoURL = auth.auth.photoURL;
        } else if (auth) {
          this.displayName = auth.auth.email;
          this.email = auth.auth.email;
          this.photoURL = "https://api.adorable.io/avatars/120/"+ auth.auth.email +".png";        
        }
      }
    });

    this.messages = this.af.database.list('messages');

  }

  // getAll(chatId) {
  //   const messages = MESSAGES.filter (message => message.chatId === chatId);
  //   return Promise.resolve(messages);
  // }

  setSearchValue(value: string): void {
    this.search$.next(value);
  }

  getSearchValue(): BehaviorSubject<string> {
    return this.search$;
  }

   sendMessage(text, id) {
     var message = {
       chatId: id,
       text: text,
       displayName: this.displayName,
       email: this.email,
       photoURL: this.photoURL,
       sentAt: Date.now()
     };
     this.messages.push(message);
    //  console.log(message);
  }

}
