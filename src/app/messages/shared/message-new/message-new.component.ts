import { Component, OnInit } from '@angular/core';
import { MessageService } from "../message.service";
import { ActivatedRoute, Params } from '@angular/router';

@Component({
  selector: 'ct-message-new',
  styleUrls: ['./message-new.component.scss'],
  templateUrl: './message-new.component.html'
})

export class MessageNewComponent implements OnInit {
  newMessage: string = '';
  chatId: number;

  constructor(public messageService: MessageService,
              private route: ActivatedRoute) {}

  ngOnInit() {
    this.route.params.subscribe((params: Params) => {
      this.chatId = +params['id'];
      console.log(this.chatId);
    });
  }
  
  sendMessage() {
    if (this.newMessage.replace(/\r?\n|\r/g, " ").trim() != '' ) {
      this.messageService.sendMessage(this.newMessage, this.chatId);
      this.newMessage = '';  
    }
  }

}
