import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router";
import { AngularFire, AuthProviders, AuthMethods } from 'angularfire2';


@Component({
  selector: 'ct-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.scss']
})

export class NavComponent implements OnInit {
  name: any;
  authenticated: boolean = false;

  constructor(public af: AngularFire,
              private router: Router) {

    this.af.auth.subscribe(auth => {
      if(auth) {
        this.authenticated = true;
        this.name = auth;
      }
    });
 }

  logout() {
    this.af.auth.logout();
    this.authenticated = false;
    console.log('Logged out');
    this.router.navigateByUrl('auth/login');
  }

  ngOnInit() { }

}
