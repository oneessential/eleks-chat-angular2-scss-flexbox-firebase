import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { map } from "rxjs/operator/map";
import { API_CONFIG } from "../shared/api.config";
import { BehaviorSubject } from "rxjs/BehaviorSubject";

@Injectable()
export class UsersService {
    private search$: BehaviorSubject<string> = new BehaviorSubject('');

    constructor(private http: Http){}

    public getUsers() {
        return this.http.get(API_CONFIG.USERS)
            .map(response => response.json());
    }

    setSearchValue(value: string): void {
        this.search$.next(value);
    }

    getSearchValue(): BehaviorSubject<string> {
        return this.search$;
    }

};

