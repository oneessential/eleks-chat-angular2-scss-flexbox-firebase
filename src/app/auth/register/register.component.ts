import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AngularFire, AuthProviders, AuthMethods } from 'angularfire2';
import { Router } from '@angular/router';

@Component({
  selector: 'ct-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss'],
})

export class RegisterComponent implements OnInit {
  user: FormGroup;
  error: any;

  constructor(public af: AngularFire,
              private fb: FormBuilder,
              private router: Router) {}

  ngOnInit() {
    this.user = this.fb.group({
      // name: ['', [Validators.required, Validators.minLength(2)]],
      email: ['', [Validators.required, Validators.pattern("[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?")]],
      passwords: this.fb.group({
        password: ['', [Validators.required, Validators.minLength(6)]]
        // ,
        // confirmPassword: ['', [Validators.required, Validators.minLength(6)]]
      })
    });
  }

  onSubmit(formData) {
      if(formData.valid) {
        // console.log(formData.value);
        this.af.auth.createUser({
          email: formData.value.email,
          password: formData.value.passwords.password
        }).then(
          (success) => {
          console.log(success);
          this.router.navigate(['/chat'])
        }).catch(
          (err) => {
          console.log(err);
          this.error = err;
        })
      }
    }
  // onSubmit(user) {
  //   // console.log(user.value, user.valid);
  //   // this.authService.register(user.value);
  // }
}
