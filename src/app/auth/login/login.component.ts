import { Component, OnInit, HostBinding } from '@angular/core';
import { AngularFire, AuthProviders, AuthMethods } from 'angularfire2';
import { User } from './login.interface';
import { Router } from '@angular/router';

@Component({
  selector: 'ct-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})

export class LoginComponent implements OnInit {

  user: User = {
    email: "",
    password: ""
  }
  error: any;

  constructor(public af: AngularFire, 
              private router: Router) {

    this.af.auth.subscribe(auth => { 
      if(auth) {
        this.router.navigateByUrl('/chat');
      }
    });
  }

  onSubmit(formData) {
    if(formData.valid) {
      // console.log(formData.value);
      this.af.auth.login({
        email: formData.value.email,
        password: formData.value.password
      },
      {
        provider: AuthProviders.Password,
        method: AuthMethods.Password,
      }).then(
        (success) => {
        console.log(success);
        this.router.navigate(['/chat']);
      }).catch(
        (err) => {
        console.log(err);
        this.error = err;
      })
    }
  }

  loginFb() {
    this.af.auth.login({
      provider: AuthProviders.Facebook,
      method: AuthMethods.Popup,
    }).then(
        (success) => {
        this.router.navigate(['/chat']);
      }).catch(
        (err) => {
        this.error = err;
      })
  }

  loginGoogle() {
    this.af.auth.login({
      provider: AuthProviders.Google,
      method: AuthMethods.Popup,
    }).then(
        (success) => {
        this.router.navigate(['/chat']);
      }).catch(
        (err) => {
        this.error = err;
      })
  }

  ngOnInit() {  
      
  }

}
