import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpModule } from '@angular/http';
import { AppRoutingModule } from './app-routing.module';
import { CoreModule } from './core/core.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './home.component';
import { PageNotFoundComponent } from './not-found.component';
import { UsersService } from "./core/users.service";
import { AngularFireModule } from 'angularfire2';

export const firebaseConfig = {
  apiKey: "AIzaSyBzYoetync4QA6uiVk5svXtl1ep3l0sqkk",
  authDomain: "eleks-chat.firebaseapp.com",
  databaseURL: "https://eleks-chat.firebaseio.com",
  projectId: "eleks-chat",
  storageBucket: "eleks-chat.appspot.com",
  messagingSenderId: "555892099325"
};

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    PageNotFoundComponent
  ],
  imports: [
    BrowserModule,
    CoreModule,
    AppRoutingModule,
    HttpModule,
    AngularFireModule.initializeApp(firebaseConfig)
  ],
  providers: [UsersService],
  bootstrap: [AppComponent]
})

export class AppModule { }
