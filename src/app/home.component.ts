import { Component, OnInit } from '@angular/core';
import { AngularFire, AuthProviders, AuthMethods } from 'angularfire2';

@Component({
  template: '<h1>{{ name }} welcome to Chat Application</h1>',
  styles: [':host { display: flex; flex-direction: column; flex-grow: 1; justify-content: center; margin: auto;}']
})

export class HomeComponent implements OnInit {

  name: any = 'Hello';

  constructor(public af: AngularFire){

    this.af.auth.subscribe(auth => {
      if(auth && auth.auth.displayName != null) {
        this.name = auth.auth.displayName;
      }
    });
  }

  ngOnInit() {

  }

}
