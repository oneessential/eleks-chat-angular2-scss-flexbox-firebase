import { Directive, ElementRef, HostListener, Input, Output, EventEmitter } from '@angular/core';

@Directive({
  selector: '[myHighlight]'
})
export class HighlightDirective {
  numberOfClicks = 0;

  constructor(private el: ElementRef) { }

  @Input('myHighlight') highlightColor: string;

  // @Output()
  //   public clickOutside = new EventEmitter<MouseEvent>();
  //
  //   @HostListener('document:click', ['$event', '$event.target'])
  //   public onOutClick(event: MouseEvent, targetElement: HTMLElement): void {
  //       if (!targetElement) {
  //           return;
  //       }
  //
  //       const clickedInside = this.el.nativeElement.contains(targetElement);
  //       if (!clickedInside) {
  //           this.clickOutside.emit(event);
  //           console.log(event);
  //       }
  //   }

  @HostListener('click', ['$event.target'])
  onClick(target) {
    // if (!this.el.nativeElement.contains(target)) {
    //   this.highlight(null);
    // }
    if(this.numberOfClicks == 0) {
      this.highlight(this.highlightColor || '#80CBC4');
      this.numberOfClicks++;
      console.log(target);
    } else if (this.numberOfClicks >= 1) {
      this.highlight(null);
      this.numberOfClicks = 0;
    }
  }

  private highlight(color: string) {
    this.el.nativeElement.style.backgroundColor = color;
  }
}
