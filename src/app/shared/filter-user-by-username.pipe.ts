import { Pipe, PipeTransform } from '@angular/core';
import { User } from '../core/user.model';
 
@Pipe({
	name: 'filterUserByUsername'
})

export class FilterUserByUsernamePipe implements PipeTransform {
	public transform(users: User[], filterValue: string) {
		if (users) {
			return users.filter(user => {
				return user.username.match(new RegExp(filterValue, 'gi'));
			});
		} else {
			return [];
		}
	}
}