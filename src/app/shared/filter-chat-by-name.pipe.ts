import { Pipe, PipeTransform } from '@angular/core';
import { Chat } from '../chats/shared/chat.model'

@Pipe({
  name: 'filterChatByName'
})
export class FilterChatByNamePipe implements PipeTransform {

  transform(chats: Chat[], filterValue: string): any {
    if (chats) {
      return chats.filter(chat => {
        return chat.name.match(new RegExp(filterValue, 'gi'));
      });
    } else {
      return chats;
    }
  }

}
